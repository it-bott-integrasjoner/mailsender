import threading
from aiosmtpd.controller import Controller

import pytest

from mailsender.__main__ import get_config, configure_logging
from mailsender.client import MailSender, get_client
from mailsender.models import MessageParameters, MailSenderConfig


class CustomHandler:
    async def handle_DATA(self, server, session, envelope):
        return "250 OK"


@pytest.fixture(scope="module")
def debugging_server():
    handler = CustomHandler()
    controller = Controller(handler, hostname="127.0.0.1", port=1025)
    controller.start()
    yield controller
    controller.stop()


@pytest.fixture
def client(debugging_server):
    return MailSender(
        MailSenderConfig(
            smtp_server="localhost",
            smtp_port=1025,
            sending_disabled=False,
            message_parameters=MessageParameters(
                sender_name="MailSender",
                sender_mail="noreply@uib.no",
                recipients=["debugging@uib.no"],
                subject="An error occured",
            ),
            message_types={
                "Test": MessageParameters(
                    recipients=["configured@example.com"],
                    subject="Test message text",
                )
            },
        )
    )


@pytest.fixture
def client_intg():
    config = get_config("config.json")
    configure_logging(config)
    return get_client(MailSenderConfig(**config.get("client")))


def pytest_collection_modifyitems(config, items):
    if config.option.keyword or config.option.markexpr:
        return
    skip_integration = pytest.mark.skip(
        reason='Not running with pytest -m "integration"'
    )
    for item in items:
        if "integration" in item.keywords:
            item.add_marker(skip_integration)
