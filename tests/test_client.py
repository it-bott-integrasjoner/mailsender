import pytest
import os

from mailsender.client import MailSender
from mailsender.models import MessageParameters, MailSenderConfig, Attachment


def get_file(name):
    here = os.path.realpath(os.path.join(os.getcwd()))
    with open(os.path.join(here, "tests/fixtures", name), "rb") as f:
        data = f.read()
    return data


def test_send_mail(client):
    client.send_email("hello")


def test_send_mail_with_attachment(debugging_server):
    client = MailSender(
        MailSenderConfig(
            smtp_server="localhost",
            smtp_port=1025,
            sending_disabled=False,
            message_parameters=MessageParameters(
                sender_name="MailSender", sender_mail="noreply@uio.no"
            ),
            message_types={
                "Test": MessageParameters(
                    recipients=["mohammad.kabir@uit.no"],
                    subject="Attachment test",
                )
            },
        )
    )

    content1 = get_file("sample.pdf")
    attach1 = Attachment(content=content1, file_name="sample.pdf")
    content2 = get_file("image1.jpg")
    attach2 = Attachment(content=content2, file_name="image1.jpg")

    sent = client.send_email("Test From attachment", "Test", [attach1, attach2])
    assert sent is True


def test_invalid_send_mail(debugging_server):
    client = MailSender(
        MailSenderConfig(
            smtp_server="localhost",
            smtp_port=1025,
            sending_disabled=False,
        )
    )
    with pytest.raises(ValueError) as error:
        client.send_email("hello")
    assert error.value.args[0] == "Message parameters is not set for None"


def test_send_mail_with_type(debugging_server):
    client = MailSender(
        MailSenderConfig(
            smtp_server="localhost",
            smtp_port=1025,
            sending_disabled=False,
            message_parameters=MessageParameters(
                sender_name="MailSender", sender_mail="noreply@uio.no"
            ),
            message_types={
                "Test": MessageParameters(
                    recipients=["mohammad.kabir@uit.no"], subject="An error occured"
                )
            },
        )
    )
    sent = client.send_email("hello", "Test")
    assert sent is True


def test_send_mail_with_type_disalbed():
    client = MailSender(
        MailSenderConfig(
            sending_disabled=True,
            smtp_server="localhost",
            smtp_port=1025,
            message_parameters=MessageParameters(
                sender_name="MailSender", sender_mail="noreply@uio.no"
            ),
            message_types={
                "Test": MessageParameters(
                    recipients=["mohammad.kabir@uit.no"], subject="An error occured"
                )
            },
        )
    )
    sent = client.send_email("hello", "Test")
    assert sent is False


def test_send_mail_with_invalid_type(debugging_server):
    client = MailSender(
        MailSenderConfig(
            smtp_server="localhost",
            smtp_port=1025,
            sending_disabled=False,
            message_parameters=MessageParameters(
                sender_name="MailSender",
                sender_mail="noreply@uio.no",
                subject="An error occured",
            ),
            message_types={
                "Test": MessageParameters(
                    recipients=["mohammad.kabir@uit.no"], subject="An error occured"
                )
            },
        )
    )
    with pytest.raises(ValueError) as error:
        client.send_email("hello", "Invalid test")
    assert error.value.args[0] == "No recipients for the email defined"


def test_send_mail_with_missing_subject(debugging_server):
    client = MailSender(
        MailSenderConfig(
            smtp_server="localhost",
            smtp_port=1025,
            sending_disabled=False,
            message_parameters=MessageParameters(
                sender_name="MailSender",
                sender_mail="noreply@uio.no",
                recipients=["mohammad.kabir@uit.no"],
            ),
        )
    )
    with pytest.raises(ValueError) as error:
        client.send_email("hello")
    assert error.value.args[0] == "subject not set for None"


def test_send_mail_with_special_addresses(client):
    email_addresses = ["nobody@example.com"]
    sent = client.send_email_to_specific_addresses(
        "Test_message", email_addresses, "Test"
    )
    assert sent
    content1 = get_file("sample.pdf")
    attach1 = Attachment(content=content1, file_name="sample.pdf")
    content2 = get_file("image1.jpg")
    attach2 = Attachment(content=content2, file_name="image1.jpg")

    sent = client.send_email_to_specific_addresses(
        "Test from specific address", email_addresses, "Test", [attach1, attach2]
    )

    assert sent is True


def test_send_mail_idempotent(client):
    """Verify message parameters are left alone when using methods"""
    assert client.message_parameters.recipients == ["debugging@uib.no"]

    client.send_email_to_specific_addresses(
        "Test from specific address",
        ["test@example.com"],
        "Test",
    )
    # Verify default parameters are unchanged
    assert client.message_parameters.recipients == ["debugging@uib.no"]
    # Verify parameters for message types are unchanged
    assert client.message_types["Test"].recipients == ["configured@example.com"]
