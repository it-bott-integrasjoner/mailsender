import pytest

"""
To run integration tests, set credentials in config.json and run

    python -m pytest -m integration

Tests might depend on each other (create/delete)
"""


@pytest.mark.integration
def test_send_mail(client_intg):
    client_intg.send_email("This is a test")


@pytest.mark.integration
def test_send_mail_type_test(client_intg):
    client_intg.send_email("This is a test of test", "test")
