# mailsender

Mailsender sends mail, and can be used for error reporting.
Sender, recipients and subjects can be configured either general, per message type or a mix.
Message type can be included for each call to send_email, and specific parameters is preferred.

## Usage

For the examples below, you can listen to messages by running a debug server in a terminal
```
Test by running `python -m smtpd -c DebuggingServer -n localhost:1025` in a terminal
```

Not specifying type:
```python
from mailsender.client import MailSender
from mailsender.models import MessageParameters

client = MailSender(
    smtp_server='localhost',
    smtp_port=1025,
    message_parameters=MessageParameters(
        sender_name='MailSender',
        sender_mail='noreply@uib.no',
        recipients=['debugging@uib.no'],
        subject='An error occured'
    )
)
client.send_email('Something went wrong')
```

Specifying two types, and using some general parameters:
```python
from mailsender.client import MailSender
from mailsender.models import MessageParameters

client = MailSender(
    smtp_server="localhost",
    smtp_port=1025,
    message_parameters=MessageParameters(
        sender_name='MailSender',
        sender_mail='noreply@uib.no'
    ),
    message_types={
        "First function": MessageParameters(
            recipients=['debugging1@uib.no'],
            subject='An error occured in first function'),
        "Second function": MessageParameters(
            recipients=['debugging2@uib.no'],
            subject='An error occured in second function')
    }
)
client.send_email('Something went wrong', 'First function')
client.send_email('Something went wrong', 'Second function')
```

## Tests

### Unit tests

```sh
python -m pytest
```

### Integration tests

Configure smtp server and message parameters in `config.json` 
You can either configure general message parameters or per type (the `message_type` parameter to `send_email`).
Then run

```sh
python -m pytest -m integration
```
