"""Client for sending email"""
import logging
import smtplib
from email.header import Header
from email.mime.text import MIMEText
from typing import Optional, List, Literal
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart

from mailsender.models import MessageParameters, MailSenderConfig, Attachment

logger = logging.getLogger(__name__)


class MailSender:
    def __init__(self, config: MailSenderConfig):
        """
        Mailsender.

        :param config: MailSenderConfig
        """
        self.sending_disabled = config.sending_disabled
        self.smtp_server = config.smtp_server
        self.smtp_port = config.smtp_port
        self.message_parameters: MessageParameters = MessageParameters.from_dict(
            config.message_parameters
        )
        if isinstance(config.message_types, dict):
            config.message_types = {
                k: MessageParameters.from_dict(v)
                for k, v in config.message_types.items()
            }
        self.message_types = config.message_types

    def get_message_parameters(
        self, message_type: Optional[str] = None
    ) -> MessageParameters:
        message_parameters = (
            MessageParameters(**self.message_parameters.dict())
            if self.message_parameters
            else None
        )
        if message_type is not None:
            if self.message_types and self.message_types.get(message_type):
                if message_parameters is None:
                    message_parameters = MessageParameters(
                        **self.message_types[message_type].dict()
                    )
                else:
                    for k, v in self.message_types[message_type].dict().items():
                        if not v:
                            continue
                        setattr(message_parameters, k, v)
        if message_parameters is None:
            raise ValueError("Message parameters is not set for %s" % message_type)
        for k, v in message_parameters.dict().items():
            if k != "recipients" and v is None:
                raise ValueError("%s not set for %s" % (k, message_type))
        return message_parameters

    def send_email(
        self,
        message: str,
        message_type: Optional[str] = None,
        attachments: Optional[List[Attachment]] = None,
    ) -> bool:
        if self.sending_disabled:
            logger.info("Sending of mails is disabled, not sending emails")
            return False
        message_parameters = self.get_message_parameters(message_type)
        recipients = message_parameters.recipients
        if not recipients:
            raise ValueError("No recipients for the email defined")

        if attachments is None:
            attachments = []
        sender = "%s <%s>" % (
            message_parameters.sender_name,
            message_parameters.sender_mail,
        )
        subject = message_parameters.subject

        return self.do_send_email(
            message,
            sender,
            recipients,
            subject,
            attachments,
        )

    def do_send_email(
        self,
        message: str,
        sender: str,
        recipients: List[str],
        subject: Optional[str],
        attachments: List[Attachment],
    ) -> Literal[True]:
        msg = MIMEMultipart()
        msg["From"] = sender
        msg["To"] = ", ".join(recipients)
        msg["Subject"] = Header(subject, "utf-8")
        msg.attach(MIMEText(message, "plain", "utf-8"))
        for f in attachments:
            msg.attach(MIMEApplication(f.content, Name=f.file_name))
        email = smtplib.SMTP(self.smtp_server, self.smtp_port)
        email.sendmail(sender, recipients, msg.as_string())
        email.quit()

        return True

    def send_email_to_specific_addresses(
        self,
        message: str,
        email_addresses: List[str],
        message_type: Optional[str] = None,
        attachments: Optional[List[Attachment]] = None,
    ) -> bool:
        if self.sending_disabled:
            logger.info("Sending of mails is disabled, not sending emails")
            return False
        if attachments is None:
            attachments = []

        message_parameters = self.get_message_parameters(message_type)

        sender = "%s <%s>" % (
            message_parameters.sender_name,
            message_parameters.sender_mail,
        )
        subject = message_parameters.subject

        recipients = email_addresses + (message_parameters.recipients or [])

        return self.do_send_email(message, sender, recipients, subject, attachments)


def get_client(config: MailSenderConfig) -> MailSender:
    return MailSender(config)
