"""
Mailsender, for sending error reports etc.
"""
import argparse
import json
import logging
import logging.config
from typing import Any, Callable, Optional, Sequence, Union

from .client import get_client
from .version import get_distribution

jsontype = Any

logger = logging.getLogger(__name__)

default_config_file = "config.json"
default_log_format = "%(levelname)s - %(name)s - %(message)s"
default_log_level = logging.DEBUG


def configure_logging(config: jsontype) -> None:
    log_config = config.get("logging", {})
    if log_config:
        logging.config.dictConfig(log_config)
    logging.basicConfig(level=default_log_level, format=default_log_format)


def get_config(filename: str) -> jsontype:
    logger.debug("loading config from %r", filename)
    with open(filename, "r") as f:
        return json.load(f)


def default_formatter(data: Any) -> str:
    return json.dumps(data, indent=2)


def default_output(data: Any) -> None:
    print(data)


parser = argparse.ArgumentParser()
parser.add_argument(
    "-v",
    "--version",
    action="version",
    version=str(get_distribution()),
)
parser.add_argument(
    "--config",
    default=default_config_file,
)
commands = parser.add_subparsers(
    title="commands",
    description="Lookups supported by this command line client",
    dest="command",
    metavar="command",
)


def command(command_name: str, **kwargs: Any) -> Callable[..., Any]:
    """
    Add a parser in `commands` for decorated function.
    """

    def wrapper(func: Callable[..., Any]) -> Callable[..., Any]:
        default_desc = func.__doc__
        if default_desc is not None:
            default_desc = default_desc.strip()
        if "help" not in kwargs:
            kwargs["help"] = default_desc
        if "description" not in kwargs:
            kwargs["description"] = default_desc
        cmd_parser = commands.add_parser(command_name, **kwargs)
        cmd_parser.set_defaults(func=func)
        setattr(func, "parser", cmd_parser)
        return func

    return wrapper


def call_command(config: jsontype, args: argparse.Namespace) -> None:
    client = get_client(config.get("client", {}))
    func = args.func
    logger.debug("client=%r, func=%r", client, func)
    args.func(client, args)


def main(inargs: Optional[Sequence[str]] = None) -> None:
    args = parser.parse_args(inargs)

    config = get_config(args.config)
    configure_logging(config)

    logger.debug("args: %r", args)

    if not hasattr(args, "func"):
        parser.error("no command given")
    call_command(config, args)


if __name__ == "__main__":
    parser.prog = "python -m mailsender"
    main()
