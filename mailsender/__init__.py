from .client import MailSender
from .version import get_distribution


__all__ = ['MailSender']
__version__ = get_distribution().version
