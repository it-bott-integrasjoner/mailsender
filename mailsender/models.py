from typing import Any, Dict, TypeVar, Type, Optional, overload, Union

import pydantic.generics
from pydantic import typing

NameType = TypeVar("NameType")
T = TypeVar("T", bound="BaseModel")


class BaseModel(pydantic.BaseModel):
    @overload
    @classmethod
    def from_dict(cls: Type[T], data: Dict[str, Any]) -> T:
        ...

    @overload
    @classmethod
    def from_dict(cls: Type[T], data: Any) -> Any:
        ...

    @classmethod
    def from_dict(cls: Type[T], data: Any) -> Any:
        if isinstance(data, dict):
            return cls(**data)
        else:
            return data


class Attachment(BaseModel):
    content: bytes
    file_name: str


class MessageParameters(BaseModel):
    sender_name: typing.Optional[str] = None
    sender_mail: typing.Optional[str] = None
    recipients: typing.Optional[typing.List[str]] = None
    subject: typing.Optional[str] = None


class MailSenderConfig(BaseModel):
    """
    :param smtp_server: SMTP server, e.g. smtp.uio.no
    :param smtp_port: SMTP port, e.g. 25
    :param message_parameters: Default message parameters, to use when no specific parametes is set per type
    :param message_types: Message parameters per message type
    """

    smtp_server: str
    smtp_port: int
    message_parameters: Optional[MessageParameters] = None
    message_types: Optional[typing.Dict[str, MessageParameters]] = None
    sending_disabled: Optional[bool] = True
